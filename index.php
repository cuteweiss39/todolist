  
  <?php
  include("DB.php");
  ?>
<!DOCTYPE html>
<html lang="zh-TW">
<head>
<title>todolist</title>
<link rel="stylesheet" type="text/css" href="main.css" >
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css">

</head>

  <body>



<div class="heading">
   <i class="fas fa-quidditch fa-2x" style="color: #ff922b;"></i>
   <h2>TODOLIST</h2>

</div>

<form method="POST" action="index.php">
<?php if(isset($error)) { ?>
  <p><?php echo $error ?></p>
  <?php } ?>
<input type="text" name="task" class="task_input">
<button type="submit" class="task_btn" name="submit">ADDLIST</button>
</form>

<table>
<thead>
<tr>
 <th>NO.</th>
<th>done</th>
<th>Todo</th>
<th>Delete</th>
</tr>
</thead>
<tbody>
  <?php $i=1; while ($row=mysqli_fetch_array($tasks)){ ?>
  <tr>

    <td class="num">
      <?php echo $i; ?>
  </td>
    <td class="checkbtn">
    <input type="checkbox" id="check<?php echo $i; ?>" onclick="done(<?php echo $i; ?>)">
    <label for="check<?php echo $i;?>"></label>
  </td>

  <td>
    <label for="check<?php echo $i;?>" id="label<?php echo $i; ?>">

      <?php echo $row['task'];?>
 
  </label>
</td>
    <td class="delete">
      <a href="DB.php?del_task=<?php echo $row['id']; ?>">X</a>
  </td>
  </tr>
  <?php $i++;} ?>
</tbody>
</table>

<script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.0.js"></script>
<script src="main.js"></script>

</body>

</html>
